import React, {useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {Box, Button, Divider, Menu, MenuItem} from '@material-ui/core'
import {addLayer, getLayers} from '../store/layers'
import BaseLayerEditor from './BaseLayerEditor'
import CellTowersLayerEditor from './CellTowersLayerEditor'
import WifiNetworksLayerEditor from './WifiNetworksLayerEditor'
import './LayersPanel.css'

function DataLayerPanel({layer}) {
    return (
        <>
            <Divider/>
            <Box px={2} py={1} mb={1}>
                {layer.type === 'cell-towers' && <CellTowersLayerEditor layer={layer}/>}
                {layer.type === 'wifi-networks' && <WifiNetworksLayerEditor layer={layer}/>}
            </Box>
        </>
    )
}

function AddMenuButton({handleAddLayer}) {
    const [anchorEl, setAnchorEl] = React.useState(null)

    const handleButtonClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    const handleMenuItemClick = (event, layerType) => {
        setAnchorEl(null)
        handleAddLayer(event, layerType)
    }

    return (
        <Box mt={1}>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleButtonClick}>
                <i className="fas fa-plus" style={{marginRight: 7}}/>Add Layer...
            </Button>
            <Menu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}>
                <MenuItem onClick={(event) => handleMenuItemClick(event, 'cell-towers')}>Cell Towers</MenuItem>
                <MenuItem onClick={(event) => handleMenuItemClick(event, 'wifi-networks')}>WiFi Networks</MenuItem>
            </Menu>
        </Box>
    )
}

function LayersPanel() {
    const layers = useSelector(getLayers)
    const dispatch = useDispatch()

    const baseLayer = layers[0]
    const dataLayers = layers.slice(1)

    const dataLayerPanels = dataLayers.map(layer => <DataLayerPanel key={layer.id} layer={layer}/>)

    const handleAddLayer = useCallback((event, layerType) => {
        dispatch(addLayer(layerType))
    }, [dispatch])

    return (
        <Box className="layers-panel">
            <Box mb={1} px={2} py={1}>
                <BaseLayerEditor layer={baseLayer}/>
            </Box>
            {dataLayerPanels}
            <Divider/>
            <Box my={1} pl={1}>
                <AddMenuButton handleAddLayer={handleAddLayer}/>
            </Box>
        </Box>
    )
}

export default LayersPanel