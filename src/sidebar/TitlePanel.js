import {Box} from '@material-ui/core'
import React from 'react'
import './TitlePanel.css'

function TitlePanel() {
    return (
        <Box className="title-panel">
            <div className="app-title">SYSTOLIC Technology Demo</div>
        </Box>
    )
}

export default TitlePanel