import _ from 'lodash'
import React, {useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {
    Box,
    Button,
    Chip,
    FormControl,
    IconButton,
    Menu,
    MenuItem,
    Select,
    Slider,
    TextField,
    Tooltip,
    Typography
} from '@material-ui/core'
import LayersIcon from '@material-ui/icons/Layers'
import {Autocomplete} from '@material-ui/lab'
import {
    removeLayer,
    removeLayerFilter,
    setLayerFilter,
    setLayerHue,
    setLayerOpacity,
    setLayerStyle
} from '../store/layers'
import {getTotalCounts} from '../store/wifiNetworks'

function OpacityField({opacity, handleOpacityChange}) {
    return (
        <Box>
            <FormControl fullWidth>
                <Typography variant={'subtitle2'}>Opacity</Typography>
                <Slider min={0} max={1} step={0.01} value={opacity} onChange={handleOpacityChange}/>
            </FormControl>
        </Box>
    )
}

function StyleField({style, handleStyleChange}) {
    return (
        <Box mb={2}>
            <FormControl fullWidth>
                <Typography variant={'subtitle2'}>Style</Typography>
                <Select value={style} onChange={handleStyleChange}>
                    <MenuItem value={'heatmap'}>Heatmap</MenuItem>
                </Select>
            </FormControl>
        </Box>
    )
}

function HueField({hue, handleHueChange}) {
    return (
        <Box mb={2}>
            <FormControl fullWidth>
                <Typography variant={'subtitle2'}>Hue</Typography>
                <div className="hue-gradient"/>
                <Slider min={-180} max={180} step={1} value={hue} onChange={handleHueChange}/>
            </FormControl>
        </Box>
    )
}

function AddMenuButton({layer, handleAddFilter}) {
    const [anchorEl, setAnchorEl] = React.useState(null)

    const handleButtonClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    const handleMenuItemClick = (event, filterType) => {
        setAnchorEl(null)
        handleAddFilter(event, filterType)
    }

    const menuItems = _.sortBy(Object.keys(layer.availableFilters), key => layer.availableFilters[key].title).map(filterType => (
        <MenuItem key={filterType} onClick={(event) => handleMenuItemClick(event, filterType)}
                  disabled={Boolean(layer.filters.find(filter => filter.type === filterType))}>
            {layer.availableFilters[filterType].title}
        </MenuItem>
    ))

    return (
        <Box mt={1}>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleButtonClick}>
                <i className="fas fa-plus" style={{marginRight: 7}}/>Add Filter...
            </Button>
            <Menu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}>
                {menuItems}
            </Menu>
        </Box>
    )
}

function Filters({layer, handleFilterChange, handleAddFilter, handleRemoveFilter}) {
    const filters = layer.filters.map(filter => <FilterField key={filter.type} layer={layer} filter={filter}
                                                             handleFilterChange={handleFilterChange}
                                                             handleRemoveFilter={handleRemoveFilter}/>)

    return (
        <Box>
            <div className="filters-heading">Filters</div>
            {filters}
            <AddMenuButton layer={layer} handleAddFilter={handleAddFilter}/>
        </Box>
    )
}

function FilterField({layer, filter, handleFilterChange, handleRemoveFilter}) {
    const availableFilter = layer.availableFilters[filter.type]
    const availableValues = availableFilter.availableValues || []
    return (
        <Box mt={1} className="filter-field">
            <FormControl fullWidth>
                <Typography variant={'subtitle2'}>{availableFilter.title}</Typography>
                <div className="filter-field-body">
                    {!availableFilter.range &&
                    <Autocomplete
                        className="filter-input"
                        multiple
                        options={availableValues}
                        value={filter.values}
                        freeSolo
                        onChange={(event, value, reason) => handleFilterChange(event, filter.type, value)}
                        renderTags={(value, getTagProps) =>
                            value.map((option, index) => (
                                <Chip size="small" variant="outlined" label={option} {...getTagProps({index})} />
                            ))
                        }
                        renderInput={(params) => (
                            <TextField {...params} />
                        )}/>
                    }
                    {availableFilter.range &&
                    <Slider min={availableFilter.min} max={availableFilter.max} style={{marginRight: 10}}
                            defaultValue={[availableFilter.min, availableFilter.max]} valueLabelDisplay="auto"
                            onChangeCommitted={(event, value) => handleFilterChange(event, filter.type, value)}/>
                    }
                    <Tooltip title="Remove Filter">
                        <IconButton className="remove-filter" size="small" edge="end"
                                    onClick={(event) => handleRemoveFilter(event, filter.type)}>
                            <i className="fas fa-trash-alt"></i>
                        </IconButton>
                    </Tooltip>
                </div>
            </FormControl>
        </Box>
    )
}

function WifiNetworksLayerEditor({layer}) {
    const {label, opacity, style, hue} = layer
    const totalCounts = useSelector(getTotalCounts)
    const dispatch = useDispatch()

    const handleOpacityChange = useCallback((event, newValue) => {
        dispatch(setLayerOpacity(layer.id, newValue))
    }, [dispatch, layer])

    const handleStyleChange = useCallback((event) => {
        if (event.target.value) {
            dispatch(setLayerStyle(layer.id, event.target.value))
        }
    }, [dispatch, layer])

    const handleHueChange = useCallback((event, newValue) => {
        dispatch(setLayerHue(layer.id, newValue))
    }, [dispatch, layer])

    const handleFilterChange = useCallback((event, filterType, filterValues) => {
        dispatch(setLayerFilter(layer.id, filterType, filterValues))
    }, [dispatch, layer])

    const handleAddFilter = useCallback((event, filterType) => {
        dispatch(setLayerFilter(layer.id, filterType, []))
    }, [dispatch, layer])

    const handleRemoveFilter = useCallback((event, filterType) => {
        dispatch(removeLayerFilter(layer.id, filterType))
    }, [dispatch, layer])

    const handleRemoveLayer = useCallback((event, layer) => {
        dispatch(removeLayer(layer.id))
    }, [dispatch])

    return <div className="layer-editor">
        <div className="layer-editor-heading">
            <LayersIcon/>
            {label}
            <Tooltip title="Remove Layer">
                <IconButton className="remove-layer" size="small" edge="end"
                            onClick={(event) => handleRemoveLayer(event, layer)}>
                    <i className="fas fa-trash-alt"></i>
                </IconButton>
            </Tooltip>
        </div>
        {_.isNumber(totalCounts[layer.id]) &&
            <Typography variant="h6" align="center">{totalCounts[layer.id].toLocaleString()}</Typography>
        }
        <OpacityField opacity={opacity} handleOpacityChange={handleOpacityChange}/>
        <StyleField style={style} handleStyleChange={handleStyleChange}/>
        {style === 'heatmap' &&
        <HueField hue={hue} handleHueChange={handleHueChange}/>
        }
        <Filters layer={layer} handleFilterChange={handleFilterChange} handleAddFilter={handleAddFilter}
                 handleRemoveFilter={handleRemoveFilter}/>
    </div>
}

export default WifiNetworksLayerEditor