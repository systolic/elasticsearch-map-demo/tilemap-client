import _ from 'lodash'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import TileLayer from 'ol/layer/Tile'
import VectorTileLayer from 'ol/layer/VectorTile'
import OpenLayerMap from 'ol/Map'
import Overlay from 'ol/Overlay'
import View from 'ol/View'
import XYZ from 'ol/source/XYZ'
import Icon from 'ol/style/Icon'
import Style from 'ol/style/Style'
import {fromLonLat, transform, transformExtent} from 'ol/proj'
import {defaults as defaultControls, ScaleLine} from 'ol/control'
import * as cellTowerSelectors from '../store/cellTowers'
import * as wifiNetworkSelectors from '../store/wifiNetworks'
import {getLayers} from '../store/layers'
import {wrapLongitudeTo180} from '../support/CoordinateSupport'
import RasterLayerSource from './RasterLayerSource'
import IconLayerSource from './IconLayerSource'
import 'ol/ol.css'
import './Map.css'

const ICON_LAYER_ZOOM_THRESHOLD = 15

class Map extends Component {

    constructor(props) {
        super(props)
        this.mapContainer = React.createRef()
        this.handleWindowResize = this.onWindowResize.bind(this)
        this.eventuallyResizeMapContainer = _.debounce(this.resizeMapContainer.bind(this), 500)
        this.state = {
            extent: undefined,
            selectionGeoPoint: undefined,
            selectionResolution: undefined
        }
    }

    handleMoveEnd = () => {
        const view = this.state.map.getView()
        const extent = transformExtent(view.calculateExtent(this.state.map.getSize()), view.getProjection().getCode(), 'EPSG:4326')

        this.setState({
            extent: extent
        })
    }

    handleClick = (event) => {
        const {showDetails} = this.props
        const {selectedLocationOverlay} = this.state

        const view = this.state.map.getView()
        const geoPoint = transform(event.coordinate, view.getProjection().getCode(), 'EPSG:4326')
        geoPoint[0] = wrapLongitudeTo180(geoPoint[0])
        const resolution = view.getResolution()

        this.setState({
            selectionGeoPoint: geoPoint,
            selectionResolution: resolution
        })

        selectedLocationOverlay.setPosition(event.coordinate)

        this.fetchDetailsForCurrentSelection()

        showDetails(geoPoint)
    }

    handleChangeResolution = (event) => {
        const resolution = event.target.get(event.key)
        const z = this.getMapLayersForLayerState(this.props.layers[0])[0].getSource().getTileGrid().getZForResolution(resolution)
        const cellTowerLayers = this.props.layers.slice(1)

        cellTowerLayers.forEach(layer => {
            const [rasterLayer, iconLayer] = this.getMapLayersForLayerState(layer)
            if (z >= ICON_LAYER_ZOOM_THRESHOLD && rasterLayer.getVisible()) {
                rasterLayer.setVisible(false)
                iconLayer.setVisible(true)
                iconLayer.once('postrender', () => _.defer(() => this.updateDataLayerHue(layer.id)))
            } else if (z < ICON_LAYER_ZOOM_THRESHOLD && iconLayer.getVisible()) {
                rasterLayer.setVisible(true)
                iconLayer.setVisible(false)
                rasterLayer.once('postrender', () => _.defer(() => this.updateDataLayerHue(layer.id)))
            }
        })
    }

    updateDataLayerHue(layerId) {
        const layer = this.props.layers.find(layer => layer.id === layerId)
        const hue = layer.hue

        const dataLayerEl = document.querySelector(`.${layerId}-data-layer`)
        if (!dataLayerEl) {
            console.warn('data layer element not found')
            return
        }

        if (layer.style === 'heatmap') {
            dataLayerEl.style.filter = `hue-rotate(${hue.toFixed()}deg)`
        } else {
            dataLayerEl.style.filter = ''
        }
    }

    initializeMap() {
        const baseMap = this.props.layers.find(layer => layer.id === 'base-map')
        const mapLayers = [this.createBaseMapLayer(baseMap), ..._.without(this.props.layers, baseMap).map(layer => [this.createCellTowersRasterLayer(layer), this.createCellTowersIconLayer(layer)]).flat()]

        const view = new View({
            center: fromLonLat([-5.05, 29.53]),
            zoom: 2
        })

        const selectedLocationOverlay = new Overlay({
            element: document.getElementById('selected-location-overlay'),
            positioning: 'center-center'
        })

        const map = new OpenLayerMap({
            target: this.mapContainer.current,
            layers: mapLayers,
            view: view,
            controls: defaultControls().extend([
                new ScaleLine()
            ]),
            overlays: [selectedLocationOverlay]
        })

        this.setState({
            map,
            selectedLocationOverlay
        })

        map.on('moveend', this.handleMoveEnd)
        map.on('click', this.handleClick)

        view.on('change:resolution', this.handleChangeResolution)
    }

    addMapLayersForLayerState(layer) {
        const {map} = this.state

        let layersToAdd
        switch (layer.type) {
            case 'cell-towers':
                layersToAdd = [this.createCellTowersRasterLayer(layer), this.createCellTowersIconLayer(layer)]
                break

            case 'wifi-networks':
                layersToAdd = [this.createWifiNetworksRasterLayer(layer), this.createWifiNetworksIconLayer(layer)]
                break

            default:
                throw new Error(`Unexpected layer type: ${layer.type}`)
        }

        layersToAdd.forEach(layer => map.addLayer(layer))

        this.fetchTotalCountsForCurrentView()
    }

    removeMapLayersForLayerState(layer) {
        const {map} = this.state
        const mapLayers = this.getMapLayersForLayerState(layer)
        mapLayers.forEach(mapLayer => map.removeLayer(mapLayer))
    }

    createBaseMapLayer(baseMap) {
        const baseMapId = 'mapbox/dark-v10'
        const accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN
        return new TileLayer({
            id: baseMap.id,
            className: 'base-layer',
            source: new XYZ({
                url: `https://api.mapbox.com/styles/v1/${baseMapId}/tiles/256/{z}/{x}/{y}?access_token=${accessToken}`
            }),
            opacity: baseMap.opacity
        })
    }

    createCellTowersRasterLayer(layer) {
        const tileLayer = new TileLayer({
            id: `${layer.id}-raster`,
            className: `data-layer ${layer.id}-data-layer`,
            opacity: layer.opacity,
            source: new RasterLayerSource({
                style: layer.style,
                filter: layer.elasticsearchFilter
            })
        })
        tileLayer.once('postrender', () => _.defer(() => this.updateDataLayerHue(layer.id)))
        return tileLayer
    }

    createCellTowersIconLayer(layer) {
        return new VectorTileLayer({
            id: `${layer.id}-icon`,
            className: `data-layer ${layer.id}-data-layer`,
            declutter: true,
            source: new IconLayerSource({
                style: layer.style,
                filter: layer.elasticsearchFilter
            }),
            style: new Style({
                image: new Icon({
                    src: 'tower.png'
                })
            }),
            visible: false
        })
    }

    createWifiNetworksRasterLayer(layer) {
        const tileLayer = new TileLayer({
            id: `${layer.id}-raster`,
            className: `data-layer ${layer.id}-data-layer`,
            opacity: layer.opacity,
            source: new RasterLayerSource({
                pathPrefix: '/wifi-networks',
                style: layer.style,
                filter: layer.elasticsearchFilter
            })
        })
        tileLayer.once('postrender', () => _.defer(() => this.updateDataLayerHue(layer.id)))
        return tileLayer
    }

    createWifiNetworksIconLayer(layer) {
        return new VectorTileLayer({
            id: `${layer.id}-icon`,
            className: `data-layer ${layer.id}-data-layer`,
            declutter: true,
            source: new IconLayerSource({
                pathPrefix: '/wifi-networks',
                style: layer.style,
                filter: layer.elasticsearchFilter
            }),
            style: new Style({
                image: new Icon({
                    src: 'wifi-network.png'
                })
            }),
            visible: false
        })
    }

    componentDidMount() {
        this.resizeMapContainer()

        this.initializeMap()

        window.addEventListener('resize', this.handleWindowResize)

        this.fetchTotalCountsForCurrentView()
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.layers.length === prevProps.layers.length) {
            prevProps.layers.forEach((prevLayer, index) => {
                const layer = this.props.layers[index]
                if (layer.opacity !== prevLayer.opacity) {
                    this.getMapLayersForLayerState(layer).forEach(mapLayer => mapLayer.setOpacity(layer.opacity))
                }
                if (layer.style && layer.style !== prevLayer.style) {
                    this.getMapLayersForLayerState(layer).forEach(mapLayer => mapLayer.getSource().setStyle(layer.style))
                    this.updateDataLayerHue(layer.id)
                }
                if (layer.hue && layer.hue !== prevLayer.hue) {
                    this.updateDataLayerHue(layer.id)
                }
                if (!_.isEqual(layer.elasticsearchFilter, prevLayer.elasticsearchFilter)) {
                    this.getMapLayersForLayerState(layer).forEach(mapLayer => mapLayer.getSource().setFilter(layer.elasticsearchFilter))
                    this.fetchTotalCountsForCurrentView()
                    this.fetchDetailsForCurrentSelection()
                }
            })
        }

        if (this.props.layers.length === prevProps.layers.length + 1) {
            this.addMapLayersForLayerState(this.props.layers[this.props.layers.length - 1])
        } else if (this.props.layers.length === prevProps.layers.length - 1) {
            const layerIds = this.props.layers.map(layer => layer.id)
            this.removeMapLayersForLayerState(prevProps.layers.find(layer => !layerIds.includes(layer.id)))
        }

        if (this.state.extent !== prevState.extent) {
            this.fetchTotalCountsForCurrentView()
        }

        if (prevProps.detailsVisible && !this.props.detailsVisible) {
            this.setState({
                selectionGeoPoint: null,
                selectionResolution: null
            })
        }
    }

    onWindowResize() {
        this.eventuallyResizeMapContainer()
    }

    resizeMapContainer() {
        this.mapContainer.current.style.height = `${window.innerHeight}px`
    }

    render() {
        return <div ref={this.mapContainer} className="map-container"></div>
    }

    getMapLayersForLayerState(layer) {
        const mapLayers = this.state.map.getLayers().getArray()
        if (layer.id === 'base-map') {
            return [mapLayers.find(mapLayer => mapLayer.get('id') === layer.id)]
        } else {
            return mapLayers.filter(mapLayer => mapLayer.get('id').startsWith(layer.id))
        }
    }

    fetchTotalCountsForCurrentView() {
        const {layers, fetchTotalCellTowerCounts, fetchTotalWifiNetworkCounts} = this.props
        const {extent} = this.state
        fetchTotalCellTowerCounts(layers, extent)
        fetchTotalWifiNetworkCounts(layers, extent)
    }

    fetchDetailsForCurrentSelection() {
        const {fetchCellTowerDetails, layers} = this.props
        const {selectionGeoPoint, selectionResolution} = this.state
        if (selectionGeoPoint && selectionResolution) {
            fetchCellTowerDetails(selectionGeoPoint, selectionResolution, layers)
        }
    }

}

const mapStateToProps = state => {
    return {
        layers: getLayers(state),
        detailsVisible: cellTowerSelectors.isDetailsVisible(state)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchTotalCellTowerCounts: (layers, extent) => dispatch(cellTowerSelectors.fetchTotalCounts(layers, extent)),
        fetchTotalWifiNetworkCounts: (layers, extent) => dispatch(wifiNetworkSelectors.fetchTotalCounts(layers, extent)),
        fetchCellTowerDetails: (geoPoint, resolution, layers) => dispatch(cellTowerSelectors.fetchDetails(geoPoint, resolution, layers)),
        showDetails: (geoPoint) => dispatch(cellTowerSelectors.showDetails(geoPoint))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Map)