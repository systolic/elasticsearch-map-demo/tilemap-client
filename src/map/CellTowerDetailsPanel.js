import _ from 'lodash'
import React, {useCallback, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {
    Box,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TableSortLabel,
    Tooltip
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import {makeStyles} from '@material-ui/core/styles'
import {getDetailsCellTowers, getDetailsSelectedLocation, hideDetails, isDetailsVisible} from '../store/cellTowers'
import {getLayers, toggleFilterValueForAllLayers} from '../store/layers'
import './CellTowerDetailsPanel.css'

const useStyles = makeStyles({
    paperRoot: {
        backgroundColor: 'hsla(0, 0%, 26%, 0.97)',
        boxShadow: '0 6px 10px hsla(0, 0%, 0%, 0.7)',
        transition: 'bottom 300ms ease'
    },
    container: {
        maxHeight: 180
    }
})

function Title({cellTowerCount, selectedLocation}) {
    if (!selectedLocation) {
        return null
    }

    const [longitude, latitude] = selectedLocation
    const label = `${cellTowerCount} Cell Tower${cellTowerCount === 1 ? '' : 's'} near ${latitude.toFixed(5)}, ${longitude.toFixed(5)}`

    return (
        <Box className="title">
            <i className="fas fa-crosshairs"/>{label}
        </Box>
    )
}

function NoCellTowersFoundMessage() {
    return (
        <div className="no-cell-towers-found-message">
            <div className="message-text">No Cell Towers Found</div>
        </div>
    )
}

const columns = [{
    id: 'radio',
    fieldName: 'radio',
    header: 'Radio Type',
    sortIteratee: true
}, {
    id: 'mcc',
    fieldName: 'mcc',
    header: 'MCC',
    sortIteratee: true
}, {
    id: 'mnc',
    fieldName: 'net',
    header: 'MNC',
    sortIteratee: true
}, {
    id: 'area',
    fieldName: 'area',
    header: 'Area',
    sortIteratee: true
}, {
    id: 'cell',
    fieldName: 'cell',
    header: 'Cell',
    sortIteratee: true
}, {
    id: 'operator',
    fieldName: 'operator',
    header: 'Operator'
}, {
    id: 'country',
    fieldName: 'countryName',
    header: 'Country'
}, {
    id: 'status',
    fieldName: 'status',
    header: 'Status'
}, {
    id: 'range',
    fieldName: 'range',
    header: 'Range (m)'
}]

function FilterIcon({type, value, active, handleToggleFilter}) {
    if (!value) { return null }

    return (
        <Tooltip title="Filter by This Value">
            <span><i className={`fas fa-filter filter ${active ? 'active' : ''}`} onClick={() => handleToggleFilter(type, value)}/></span>
        </Tooltip>
    )
}

function CellTowerTableRow({cellTower, index, cellTowersLayers, filterableColumns, handleToggleFilter}) {
    const tableCells = columns.map(column => {
        const value = cellTower[column.fieldName]
        const active = cellTowersLayers.every(layer => layer.filters.some(filter => filter.type === column.fieldName && filter.values.includes(String(value))))

        if (!value) {
            return <TableCell key={column.id}><span className="muted">&mdash;</span></TableCell>
        }

        return (
            <TableCell key={column.id}>
                {value}
                {filterableColumns.includes(column.fieldName) &&
                    <FilterIcon type={column.fieldName} value={value} active={active} handleToggleFilter={handleToggleFilter}/>
                }
            </TableCell>
        )
    })
    return (
        <TableRow key={index} hover>
            {tableCells}
        </TableRow>
    )
}

function CellTowerDetailsPanel() {
    const [sortColumn, setSortColumn] = useState('radio')
    const [sortDirection, setSortDirection] = useState('asc')

    const layers = useSelector(getLayers)
    const visible = useSelector(isDetailsVisible)
    const cellTowers = useSelector(getDetailsCellTowers)
    const selectedLocation = useSelector(getDetailsSelectedLocation)
    const dispatch = useDispatch()

    const classes = useStyles()
    const classNames = ['details-overlay']
    if (!visible) {
        classNames.push('hidden')
    }

    const cellTowersLayers = layers.filter(layer => layer.availableFilters)
    let filterableColumns = []
    if (cellTowersLayers.length > 0) {
        filterableColumns = Object.keys(cellTowersLayers[0].availableFilters).filter(filterType => !cellTowersLayers[0].availableFilters[filterType].range)
    }

    const handleToggleFilter = useCallback((filterType, filterValue) => {
        dispatch(toggleFilterValueForAllLayers(filterType, String(filterValue)))
    }, [dispatch])

    const handleClose = useCallback(() => {
        dispatch(hideDetails())
    }, [dispatch])

    const handleRequestSort = useCallback((property) => {
        const isAsc = sortColumn === property && sortDirection === 'asc'
        setSortDirection(isAsc ? 'desc' : 'asc')
        setSortColumn(property)
    }, [sortColumn, sortDirection])

    const columnHeaders = columns.map(column => (
        <TableCell key={column.id} sortDirection={sortColumn === column.id ? sortDirection : false}>
            <TableSortLabel
                active={sortColumn === column.id}
                direction={sortColumn === column.id ? sortDirection : 'asc'}
                onClick={() => handleRequestSort(column.id)}>
                {column.header}
            </TableSortLabel>
        </TableCell>
    ))

    const sortIteratees = [columns.find(column => column.id === sortColumn).fieldName].concat(columns.filter(column => column.sortIteratee && column.id !== sortColumn).map(column => column.fieldName))
    const sortedTableRows = _.sortBy(cellTowers, sortIteratees)
    if (sortDirection === 'desc') {
        _.reverse(sortedTableRows)
    }

    const tableRows = sortedTableRows.map((cellTower, index) => <CellTowerTableRow key={index} cellTower={cellTower} cellTowersLayers={cellTowersLayers} filterableColumns={filterableColumns} handleToggleFilter={handleToggleFilter}/>)

    return (
        <Paper id="details-overlay" className={classNames.join(' ')} classes={{root: classes.paperRoot}}>
            <Title cellTowerCount={cellTowers.length} selectedLocation={selectedLocation}/>
            {cellTowers.length > 0 &&
            <Box>
                <TableContainer className={classes.container}>
                    <Table size="small" stickyHeader>
                        <TableHead>
                            <TableRow>
                                {columnHeaders}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableRows}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            }
            {cellTowers.length === 0 &&
            <NoCellTowersFoundMessage/>
            }
            <IconButton className="close-button" size="small" onClick={handleClose}>
                <CloseIcon/>
            </IconButton>
        </Paper>
    )
}

export default CellTowerDetailsPanel
