import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'dark'
    },
    typography: {
        fontSize: 12
    },
    overrides: {
        MuiDivider: {
            root: {
                backgroundColor: 'hsla(0, 0%, 100%, 0.16)'
            }
        },
        MuiPaper: {
            root: {
                backgroundColor: 'hsl(0, 0%, 26%)'
            }
        },
        MuiTooltip: {
            tooltip: {
                fontSize: 11
            }
        }
    }
});

export default theme;