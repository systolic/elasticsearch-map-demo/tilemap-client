import {configureStore} from '@reduxjs/toolkit'
import cellTowers from './cellTowers'
import layers from './layers'
import wifiNetworks from './wifiNetworks'

export default configureStore({
    reducer: {
        cellTowers,
        layers,
        wifiNetworks
    }
})