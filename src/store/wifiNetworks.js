import {createSlice} from '@reduxjs/toolkit'
import {containsAllLongitudes, wrapLongitudeTo180} from '../support/CoordinateSupport'

export const wifiNetworks = createSlice({
    name: 'wifiNetworks',
    initialState: {
        totalCounts: {}
    },
    reducers: {
        updateTotalCount: {
            reducer: (state, action) => {
                const {layerId, totalCount} = action.payload
                state.totalCounts[layerId] = totalCount
            },
            prepare: (layerId, totalCount) => ({payload: {layerId, totalCount}})
        }
    }
})

export const {updateTotalCount} = wifiNetworks.actions

export const fetchTotalCounts = (layers, extent) => dispatch => {
    if (!layers || !extent) {
        return
    }

    let [west, south, east, north] = extent

    west = wrapLongitudeTo180(west)
    east = wrapLongitudeTo180(east)

    if (containsAllLongitudes(west, south, east, north)) {
        west = -180
        east = 180
    }

    layers.filter(layer => layer.type === 'wifi-networks').forEach(layer => {
        const url = new URL('/wifi-networks/count-region', window.location.origin)
        const params = {north, south, east, west}
        if (layer.elasticsearchFilter) {
            params['filter'] = JSON.stringify(layer.elasticsearchFilter)
        }
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

        fetch(url, {
            method: 'GET',
            mode: 'same-origin',
            headers: {
                'Content-Type': 'text/plain'
            }
        }).then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            response.text().then(text => {
                const totalCount = parseInt(text, 10)
                dispatch(updateTotalCount(layer.id, totalCount))
            })
        }).catch(error => {
            console.error(error)
        })
    })
}

export const getTotalCounts = state => state.wifiNetworks.totalCounts

export default wifiNetworks.reducer