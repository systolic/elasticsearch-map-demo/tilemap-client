import _ from 'lodash'
import {createSlice} from '@reduxjs/toolkit'
import ElasticsearchFilterBuilder from '../support/ElasticsearchFilterBuilder'

function createElasticsearchFilter(layer) {
    const builder = new ElasticsearchFilterBuilder()

    layer.filters.forEach(filter => {
        if (layer.availableFilters[filter.type].range) {
            if (filter.values.length === 2) {
                builder.matchRange(filter.type, filter.values[0], filter.values[1])
            }
        } else {
            builder.matchAny(filter.type, filter.values)
        }
    })

    return builder.toString()
}

function createCellTowersLayer(index) {
    return {
        id: `cell-towers-${index}`,
        type: 'cell-towers',
        label: `Cell Tower Layer ${index}`,
        opacity: 1,
        style: 'heatmap',
        hue: 0,
        availableFilters: {
            'radio': {
                title: 'Radio Type',
                availableValues: ['CDMA', 'GSM', 'LTE', 'UMTS']
            },
            'mcc': {
                title: 'MCC'
            },
            'net': {
                title: 'MNC'
            },
            'area': {
                title: 'Area'
            },
            'cell': {
                title: 'Cell'
            },
            'status': {
                title: 'Status',
                availableValues: ['Allocated', 'Not operational', 'Operational', 'Reserved', 'Unknown', 'Temporary operational']
            },
            'range': {
                title: 'Range',
                min: 0,
                max: 50000,
                range: true
            }
        },
        filters: [],
        elasticsearchFilter: null
    }
}

function createWifiNetworksLayer(index) {
    return {
        id: `wifi-networks-${index}`,
        type: 'wifi-networks',
        label: `WiFi Network Layer ${index}`,
        opacity: 1,
        style: 'heatmap',
        hue: -95,
        availableFilters: {
            'measurements': {
                title: 'Measurements',
                min: 0,
                max: 10000,
                range: true
            }
        },
        filters: [],
        elasticsearchFilter: null
    }
}

export const layers = createSlice({
    name: 'layers',
    initialState: {
        layers: [{
            id: 'base-map',
            label: 'Base Map',
            opacity: 0.5
        }, createCellTowersLayer(1)],
        nextLayerIndex: {
            'cell-towers': 2,
            'wifi-networks': 1
        }
    },
    reducers: {
        setLayerOpacity: {
            reducer: (state, action) => {
                const {layerId, opacity} = action.payload
                state.layers.find(layer => layer.id === layerId).opacity = opacity
            },
            prepare: (layerId, opacity) => ({payload: {layerId, opacity}})
        },

        setLayerStyle: {
            reducer: (state, action) => {
                const {layerId, style} = action.payload
                state.layers.find(layer => layer.id === layerId).style = style
            },
            prepare: (layerId, style) => ({payload: {layerId, style}})
        },

        setLayerHue: {
            reducer: (state, action) => {
                const {layerId, hue} = action.payload
                state.layers.find(layer => layer.id === layerId).hue = hue
            },
            prepare: (layerId, hue) => ({payload: {layerId, hue}})
        },

        setLayerFilter: {
            reducer: (state, action) => {
                const {layerId, filterType, filterValues} = action.payload
                const draftLayer = state.layers.find(layer => layer.id === layerId)

                const existingFilter = draftLayer.filters.find(filter => filter.type === filterType)
                if (existingFilter) {
                    existingFilter.values = _.sortBy(filterValues)
                } else {
                    draftLayer.filters.push({
                        type: filterType,
                        values: _.sortBy(filterValues)
                    })
                }

                draftLayer.elasticsearchFilter = createElasticsearchFilter(draftLayer)
            },
            prepare: (layerId, filterType, filterValues) => ({payload: {layerId, filterType, filterValues}})
        },

        toggleFilterValueForAllLayers: {
            reducer: (state, action) => {
                const {filterType, filterValue} = action.payload
                const draftLayers = state.layers.filter(layer => layer.availableFilters && layer.availableFilters[filterType])
                const allActive = draftLayers.every(layer => layer.filters.some(filter => filter.type === filterType && filter.values.includes(filterValue)))

                draftLayers.forEach(draftLayer => {
                    const existingFilter = draftLayer.filters.find(filter => filter.type === filterType)

                    if (allActive) {
                        existingFilter.values = _.without(existingFilter.values, filterValue)
                        if (existingFilter.values.length === 0) {
                            draftLayer.filters = _.without(draftLayer.filters, existingFilter)
                        }
                    } else {
                        if (existingFilter) {
                            if (!existingFilter.values.includes(filterValue)) {
                                existingFilter.values = _.sortBy(existingFilter.values.concat(filterValue))
                            }
                        } else {
                            draftLayer.filters.push({
                                type: filterType,
                                values: [filterValue]
                            })
                        }
                    }

                    draftLayer.elasticsearchFilter = createElasticsearchFilter(draftLayer)
                })
            },
            prepare: (filterType, filterValue) => ({payload: {filterType, filterValue}})
        },

        removeLayerFilter: {
            reducer: (state, action) => {
                const {layerId, filterType} = action.payload
                const draftLayer = state.layers.find(layer => layer.id === layerId)
                const existingFilter = draftLayer.filters.find(filter => filter.type === filterType)
                if (existingFilter) {
                    draftLayer.filters = _.without(draftLayer.filters, existingFilter)
                    draftLayer.elasticsearchFilter = createElasticsearchFilter(draftLayer)
                }
            },
            prepare: (layerId, filterType) => ({payload: {layerId, filterType}})
        },

        addLayer: (state, action) => {
            let layer
            switch (action.payload) {
                case 'cell-towers':
                    layer = createCellTowersLayer(state.nextLayerIndex[action.payload])
                    break

                case 'wifi-networks':
                    layer = createWifiNetworksLayer(state.nextLayerIndex[action.payload])
                    break

                default:
                    throw new Error(`Unexpected layer type: ${action.payload}`)
            }
            state.layers.push(layer)
            ++state.nextLayerIndex[action.payload]
        },

        removeLayer: (state, action) => {
            const existingLayer = state.layers.find(layer => layer.id === action.payload)
            if (existingLayer) {
                state.layers = _.without(state.layers, existingLayer)
            }
        }
    }
})

export const {setLayerOpacity, setLayerStyle, setLayerHue, setLayerFilter, toggleFilterValueForAllLayers, removeLayerFilter, addLayer, removeLayer} = layers.actions

export const getLayers = state => state.layers.layers

export default layers.reducer